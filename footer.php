<?php
$foOptions = get_option('fo_theme_options');
$mail = get_bloginfo('admin_email', 'display');
?>
<footer>
    <a class="email pull-left" href="mailto:<?php echo $mail; ?>" title="<?php echo $mail; ?>"><?php echo $mail; ?></a>
    <a class="twitter social pull-right" href="//twitter.com/<?php echo $foOptions[twitter]; ?>" target="_blank" rel="nofollow"><?php echo $foOptions[twitter]; ?></a>
<!--    <p class="copirating text-center">(c) 2017 <a class="fn n org url work" href="--><?php //echo $foOptions[clink]; ?><!--" target="_blank" rel="nofollow">--><?php //bloginfo('description'); ?><!--</a>, LLC</p>-->
    <p class="copirating text-center">(c) 2017 Focus One by <a class="url" href="http://www.sickindividuals.com/" target="_blank" rel="nofollow">SICK INDIVIDUALS</a>, <a class="url" href="https://prolody.com/" target="_blank" rel="nofollow">Prolody</a> and <a class="url" href="https://www.moditone.com/" target="_blank" rel="nofollow">Moditone</a></p>
</footer>
<?php wp_footer(); ?>
</body>
</html>