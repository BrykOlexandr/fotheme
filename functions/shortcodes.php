<?php
$foOptions = get_option('fo_theme_options');


// Header Section
function sc_header_section($atts, $content = null) {
  global $foOptions;

  extract(shortcode_atts([
    'header_image' => '',
    'header_image_alt' => get_bloginfo('name', 'display') . ' Header Art',
    'h1_text' => get_bloginfo('name', 'display'),
    'h2by_text' => get_bloginfo('description', 'display'),
    'button_text' => $foOptions[btn],
    'button_link' => $foOptions[btnl],
    'try_button_text' => $foOptions[tbtn],
    'try_button_link' => $foOptions[tbtnl],
    'arrow_image' => get_bloginfo('template_url', 'display') . '/images/down_arrow.png',
    'arrow_id' => '',
  ], $atts));

  $output = '';
  $output .= '<div class="container">';
  $output .= '<div class="row">';
  $output .= '<div class="header-img pull-left">';
  $output .= '<img src="' . $header_image . '" alt="' . $header_image_alt . '">';
  $output .= '</div>';
  $output .= '<div class="header-text col-lg-8 col-md-10">';
  $output .= '<h1 class="white-text text-uppercase">' . $h1_text . '</h1>';
  $output .= '<h2 class="white-text">by ' . $h2by_text . '</h2>';
  $output .= '<p>' . $content . '</p>';
  $output .= '<a type="button" class="btn btn-default btn-checkout btn-try" href="' . $try_button_link . '" target="_blank" rel="nofollow">' . $try_button_text . '</a>';
  $output .= '<a type="button" class="btn btn-default btn-checkout" href="' . $button_link . '" target="_blank" rel="nofollow">' . $button_text . '</a>';
  $output .= '</div>';
  $output .= '</div><!-- /.row -->';
  $output .= '<div class="header-down text-center">';
  if ($arrow_id) {
    $output .= '<a href="#' . $arrow_id . '">';
    $output .= '<img src="' . $arrow_image . '">';
    $output .= '</a>';
  } else {
    $output .= '<img src="' . $arrow_image . '">';
  }
  $output .= '</div>';
  $output .= '</div><!-- /.container -->';

  return $output;
}

add_shortcode('header_section', 'sc_header_section');


// Video Section
function sc_video_section($atts, $content = null) {
  $output = '';
  $output .= '<div class="container-fluid">';
  $output .= '<div class="row">';
  $output .= '<iframe width="100%" src="//www.youtube.com/embed/' . $content . '?rel=0&amp;wmode=opaque&amp;showinfo=1" frameborder="0" allowfullscreen></iframe>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

add_shortcode('video_section', 'sc_video_section');


// Features Section
function sc_features_section($atts, $content = null) {
  extract(shortcode_atts([
    'title' => 'FEATURES',
  ], $atts));

  $output = '';
  $output .= '<div class="container">';
  $output .= '<div class="row">';
  $output .= '<div class="col-md-24">';
  $output .= '<h3>' . $title . '</h3>';
  $output .= '</div><!-- /.col-md-24 -->';
  $output .= '</div><!-- /.row -->';
  $output .= '<div class="row">' . do_shortcode($content) . '</div><!-- /.row -->';
  $output .= '</div><!-- /.container -->';

  return $output;
}

add_shortcode('features_section', 'sc_features_section');


// Features Section Item Wrap
function sc_features_section_area_wrap($atts, $content = null) {
  extract(shortcode_atts([
    'class' => '',
  ], $atts));

  $output = '<div class="col-md-8 ' . $class . '">' . do_shortcode($content) . '</div><!-- /.col-md-8 -->';

  return $output;
}

add_shortcode('features_section_area_wrap', 'sc_features_section_area_wrap');


// Features Section Item
function sc_features_section_area($atts, $content = null) {
  extract(shortcode_atts([
    'area_class' => '',
    'area_title' => '',
  ], $atts));

  $icon_name = strtolower(str_replace(" ", "-", $area_title));

  $output = '';
  $output .= '<div class="features-area ' . $area_class . '">';
  $output .= '<div>';
  $output .= '<h4 class="white-text ' . $icon_name . '-icon">' . $area_title . '</h4>';
  $output .= '<p>' . $content . '</p>';
  $output .= '</div>';
  $output .= '</div><!-- /.features-area -->';

  return $output;
}

add_shortcode('features_section_area', 'sc_features_section_area');


// Specifications Section
function sc_specifications_section($atts, $content = null) {
  extract(shortcode_atts([
    'title' => 'TECH SPECS',
    'subtitle' => '',
    'image' => '',
  ], $atts));

  $output = '';
  $output .= '<div class="container specifications-title">';
  $output .= '<div class="row">';
  if ($title) {
    $output .= '<h3>' . $title . '</h3>';
  }
  if ($subtitle) {
    $start = strpos($subtitle, "{{");
    $end = strpos($subtitle, "}}");
    $output .= '<h4 class="text-center">';
    if ($start != false && $end != false) {
      $mail = substr($subtitle, $start + 2, $end - $start - 2);
      $output .= substr($subtitle, 0, $start);
      $output .= '<a class="email" href="mailto:' . $mail . '" title="' . $mail . '">' . $mail . '</a>';
      $output .= substr($subtitle, $end + 2);
    } else {
      $output .= $subtitle;
    }
    $output .= '</h4>';
  }
  $output .= '</div><!-- /.row -->';
  $output .= '</div><!-- /.container -->';
  $output .= '<div class="container-fluid">';
  $output .= '<div class="row">';
  $output .= '<img src="' . $image . '" class="specifications-img">';
  $output .= '</div><!-- /.row -->';
  $output .= '</div><!-- /.container-fluid -->';
  if ($content) {
    $output .= '<div class="container specifications-info">';
    $output .= '<div class="row">';
    $output .= '<div class="col-md-12 col-md-offset-6">';
    $output .= '<ul class="spec-list">' . do_shortcode($content) . '</ul>';
    $output .= '</div><!-- /.col-md-12 -->';
    $output .= '</div><!-- /.row -->';
    $output .= '</div><!-- /.container -->';
  }

  return $output;
}

add_shortcode('specifications_section', 'sc_specifications_section');


// Specifications Section List Item
function sc_sp_item($atts, $content = null) {
  extract(shortcode_atts([
    'value' => '',
  ], $atts));

  $output = '<li>' . $content . '<span>' . $value . '</span></li>';

  return $output;
}

add_shortcode('sp_item', 'sc_sp_item');


// Audio Preview Section
function sc_audio_preview_section($atts, $content = null) {
  extract(shortcode_atts([
    'title' => 'AUDIO PREVIEW',
  ], $atts));

  $output = '';
  $output .= '<div class="container audiopreview-title">';
  $output .= '<div class="row">';
  $output .= '<h3>' . $title . '</h3>';
  $output .= '</div><!-- /.row -->';
  $output .= '<div class="row">' . do_shortcode($content) . '</div><!-- /.row -->';
  $output .= '</div><!-- /.container -->';

  return $output;
}

add_shortcode('audio_preview_section', 'sc_audio_preview_section');


// Audio Preview Section Spotify Item
function sc_spotify($atts, $content = null) {
  $output = '<div class="col-md-8">' . $content . '</div><!-- /.col-md-8 -->';

  return $output;
}

add_shortcode('spotify', 'sc_spotify');


// Supporting Section
function sc_supporting_section($atts, $content = null) {
  extract(shortcode_atts([
    'title' => 'ARTIST SUPPORTING',
  ], $atts));

  $output = '';
  $output .= '<div class="container">';
  $output .= '<div class="row supporting-title">';
  $output .= '<h3>' . $title . '</h3>';
  $output .= '</div><!-- /.row -->';
  $output .= '<div class="row supporting-slider-wrap">';
  $output .= '<div class="supporting-slider">';
  $output .= '<div class="next"><a href="javascript:void(jQuery(\'#supporting-carousel\').slick(\'slickNext\'));"><img src="' . get_bloginfo('template_url', 'display') . '/images/right_arrow.png"></a></div>';
  $output .= '<div class="prev"><a href="javascript:void(jQuery(\'#supporting-carousel\').slick(\'slickPrev\'));"><img src="' . get_bloginfo('template_url', 'display') . '/images/left_arrow.png"></a></div>';
  $output .= '<div id="supporting-carousel">' . do_shortcode($content) . '</div><!-- /#supporting-carousel -->';
  $output .= '</div><!-- /.supporting-slider -->';
  $output .= '</div><!-- /.row -->';
  $output .= '</div><!-- /.container -->';

  return $output;
}

add_shortcode('supporting_section', 'sc_supporting_section');


// Supporting Section Item
function sc_supporting_item($atts, $content = null) {
  extract(shortcode_atts([
    'name' => '',
    'image' => '',
    'position' => '',
  ], $atts));

  $output = '';
  $output .= '<div class="supporting-item clearfix">';
  $output .= '<img src="' . $image . '" alt="' . $name . '" class="img-circle pull-left">';
  $output .= '<div class="supporting-item-info pull-left">';
  $output .= '<p>' . $content . '</p>';
  $output .= '<h5>' . $name . ', ' . $position . '</h5>';
  $output .= '</div><!-- /.supporting-item-info -->';
  $output .= '</div><!-- /.supporting-item -->';

  return $output;
}

add_shortcode('supporting_item', 'sc_supporting_item');
?>