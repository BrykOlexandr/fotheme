<?php
/* ----------------------- */
/* Disable emojicons Start */
/* ----------------------- */
add_action('init', 'disable_wp_emojicons');
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');

  // filter to remove TinyMCE emojis
  add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}

function disable_emojicons_tinymce($plugins) {
  if (is_array($plugins)) {
    return array_diff($plugins, ['wpemoji']);
  } else {
    return [];
  }
}

add_filter('emoji_svg_url', '__return_false');


/* ----------------------- */
/* Enqueue Scripts */
/* ----------------------- */
add_action('wp_enqueue_scripts', 'fo_scripts_basic');
function fo_scripts_basic() {
  wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/assets/bootstrap.min.js', ['jquery'], '20170920', true);
  wp_enqueue_script('slick-script', get_template_directory_uri() . '/js/assets/slick.min.js', ['jquery'], '20170920', true);
  wp_enqueue_script('main-script', get_template_directory_uri() . '/js/main.min.js', ['jquery', 'bootstrap', 'slick-script'], '20170920', true);
}

/* ----------------------- */
/* Deregister Script wp-embed
/* ----------------------- */
add_action('wp_footer', 'deregister_embed');
function deregister_embed() {
  wp_deregister_script('wp-embed');
}

/* ----------------------- */
/* Enqueue font */
/* ----------------------- */
add_action('wp_enqueue_scripts', 'fo_font');
function fo_font() {
  wp_enqueue_style('main-font', '//fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,600,700');
}


/* ----------------------- */
/* Enqueue Stylesheets */
/* ----------------------- */
add_action('wp_enqueue_scripts', 'fo_style_basic');
function fo_style_basic() {
  wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', ['main-font'], '20170920', 'all');
  wp_enqueue_style('bootstrap');

  wp_register_style('main-style', get_template_directory_uri() . '/style.css', ['main-font', 'bootstrap'], '20170920', 'all');
  wp_enqueue_style('main-style');

  wp_register_style('custom-style', get_template_directory_uri() . '/css/custom.css', ['bootstrap', 'main-style'], '20170920', 'all');
  wp_enqueue_style('custom-style');
}