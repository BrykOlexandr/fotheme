<?php
/* ----------------------- */
/* Create SECTION post type */
/* ----------------------- */
add_action('init', 'create_section');
function create_section() {
  $labels = [
    'name' => 'Section',
    'singular_name' => 'Section',
    'add_new' => 'Add New',
    'add_new_item' => 'Add New Section',
    'edit' => 'Edit',
    'edit_item' => 'Edit Section',
    'new_item' => 'New Section',
    'view_item' => 'View Section',
    'search_items' => 'Search Section',
    'not_found' => 'No Section found',
    'not_found_in_trash' => 'No Section found in Trash',
    'parent' => 'Parent Section',
    'menu_name' => 'Section',
  ];

  $args = [
    'label' => 'Section',
    'labels' => $labels,
    'public' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-exerpt-view',
    'capability_type' => 'page',
    'hierarchical' => false,
    'supports' => [
      'title',
      'editor',
      'page-attributes',
    ],
    'rewrite' => true,
  ];

  register_post_type('section', $args);
}

add_filter('post_updated_messages', 'section_messages');
function section_messages($messages) {
  global $post, $post_ID;

  $messages['section'] = [
    0 => '',
    1 => sprintf('Section Updated. <a href="%s">View Section</a>', esc_url(get_permalink($post_ID))),
    2 => 'Custom Field Updated.',
    3 => 'Custom Field Deleted.',
    4 => 'Section Updated.',
    5 => isset($_GET['revision']) ? sprintf('Section Restored To Revision From %s', wp_post_revision_title((int)$_GET['revision'], false)) : false,
    6 => sprintf('Section Published. <a href="%s">View Section</a>', esc_url(get_permalink($post_ID))),
    7 => 'Section Saved.',
    8 => sprintf('Section Submitted. <a target="_blank" href="%s">Preview Section</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
    9 => sprintf('Section Scheduled For: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Section</a>', date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
    10 => sprintf('Section Draft Updated. <a target="_blank" href="%s">Preview Section</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
  ];

  return $messages;
}

// Add Meta Box
add_action('admin_init', 'section_register_meta');
function section_register_meta() {
  add_meta_box('section_meta_box', 'Section Details', 'display_section_box', 'section', 'normal', 'high');
}

function display_section_box() {
  global $post;

  $section_id = get_post_meta($post->ID, 'section_id', true);
  ?>
    <div class="my_meta_control">
        <p>
            <label>HTML Section ID: <input type="text" name="section_id" value="<?php echo !empty($section_id) ? $section_id : ''; ?>" style="max-width: 100%;" size="40"/></label>
            <br>
            <span>Just write ID of SECTION tag. Also you can use such construction: TAG_NAME#ID. Example: header#my-header.</span>
        </p>
    </div>
  <?php
}

add_action('save_post', 'section_save', 10, 1);
function section_save($post_id) {
  if (isset($_POST['post_type']) == 'section') {

    $current_id = get_post_meta($post_id, 'section_id', true);
    $default_id = '';
    if (isset($_POST['section_id'])) {
      $new_id = $_POST['section_id'];
      if ($current_id) {
        if ($new_id) {
          update_post_meta($post_id, 'section_id', $new_id);
        } else {
          update_post_meta($post_id, 'section_id', $default_id);
        }
      } else {
        if ($new_id) {
          add_post_meta($post_id, 'section_id', $new_id, true);
        } else {
          add_post_meta($post_id, 'section_id', $default_id);
        }
      }
    }
  }

  return $post_id;
}

// Register column menu_order
add_filter("manage_section_posts_columns", "add_menu_order_column", 4);
function add_menu_order_column($columns) {
  $num = 1;
  $new_columns = [
    'menu_order' => 'Order',
  ];

  return array_slice($columns, 0, $num) + $new_columns + array_slice($columns, $num);
}

// width column
add_action('admin_head', 'add_menu_order_column_css');
function add_menu_order_column_css() {
  echo '<style type="text/css">.column-menu_order{width:10%;}</style>';
}

// Show data in this column
add_action("manage_section_posts_custom_column", "fill_menu_order_column", 5, 2);
function fill_menu_order_column($column) {
  global $post;
  if ("menu_order" === $column) {
    echo $post->menu_order;
  }
}

// Register sortable column
add_filter('manage_edit-section_sortable_columns', 'add_menu_order_sortable_column');
function add_menu_order_sortable_column($sortable_columns) {
  $sortable_columns['menu_order'] = 'menu_order';

  return $sortable_columns;
}

?>