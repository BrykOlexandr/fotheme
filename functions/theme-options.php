<?php
add_action('admin_init', 'theme_options_init');
function theme_options_init() {
  register_setting('fo_options', 'fo_theme_options');
}

add_action('admin_menu', 'theme_options_add_page');
function theme_options_add_page() {
  add_theme_page('Theme Options', 'Theme Options', 'edit_theme_options', 'theme_options', 'theme_options_do_page');
}

function theme_options_do_page() {
  global $select_options;
//  echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/functions/theme-options-css.css">';
//  echo '<script src="' . get_template_directory_uri() . '/functions/theme-options-scripts.min.js"></script>';
  if (!isset($_REQUEST['settings-updated'])) $_REQUEST['settings-updated'] = false;
  $option_fields = [];
  ?>
    <div class="wrap">
        <style>
            form p textarea,
            form p input {
                max-width: 100%;
            }
        </style>
        <div class="metabox-holder">
            <h2>Theme Options</h2>
          <?php if (false !== $_REQUEST['settings-updated']) : ?>
              <div id="message" class="updated fade">
                  <p><strong>Options saved</strong></p>
              </div>
          <?php endif; ?>
        </div>
    </div>
    <form method="POST" action="options.php">
      <?php settings_fields('fo_options'); ?>
      <?php $options = get_option('fo_theme_options'); ?>
        <p>
            <label for="fo_theme_options[analytics]">Analytics Tracking Code HEAD:</label>
            <br>
            <textarea name="fo_theme_options[analytics]" id="fo_theme_options[analytics]" rows="10" cols="100"><?php echo $options[analytics]; ?></textarea>
        </p>
        <p>
            <label for="fo_theme_options[banalytics]">Analytics Tracking Code BODY:</label>
            <br>
            <textarea name="fo_theme_options[banalytics]" id="fo_theme_options[banalytics]" rows="5" cols="100"><?php echo $options[banalytics]; ?></textarea>
        </p>
        <p>
            <label for="fo_theme_options[mdescr]">Meta Description:</label>
            <br>
            <textarea name="fo_theme_options[mdescr]" id="fo_theme_options[mdescr]" rows="5" cols="100"><?php echo $options[mdescr]; ?></textarea>
        </p>
        <p>
            <label for="fo_theme_options[mkey]">Meta Keywords:</label>
            <br>
            <textarea name="fo_theme_options[mkey]" id="fo_theme_options[mkey]" rows="3" cols="100"><?php echo $options[mkey]; ?></textarea>
        </p>
        <p>
            <label for="fo_theme_options[plogo]">Product Logo URL:</label>
            <br>
            <input type="text" name="fo_theme_options[plogo]" id="fo_theme_options[plogo]" size="100" value="<?php echo $options[plogo]; ?>">
        </p>
        <p>
            <label for="fo_theme_options[clogo]">Company Logo URL:</label>
            <br>
            <input type="text" name="fo_theme_options[clogo]" id="fo_theme_options[clogo]" size="100" value="<?php echo $options[clogo]; ?>">
        </p>
        <p>
            <label for="fo_theme_options[clink]">Company page URL:</label>
            <br>
            <input type="text" name="fo_theme_options[clink]" id="fo_theme_options[clink]" size="100" value="<?php echo $options[clink]; ?>">
        </p>
        <p>
            <label for="fo_theme_options[btn]">Buy button text:</label>
            <br>
            <input type="text" name="fo_theme_options[btn]" id="fo_theme_options[btn]" size="100" value="<?php echo $options[btn]; ?>">
            <br>
            <span style="opacity: 0.8;">Example: Buy now for €49.95</span>
        </p>
		<p>
            <label for="fo_theme_options[tbtn]">Try button text:</label>
            <br>
            <input type="text" name="fo_theme_options[tbtn]" id="fo_theme_options[tbtn]" size="100" value="<?php echo $options[tbtn]; ?>">
            <br>
            <span style="opacity: 0.8;">Example: Try now</span>
        </p>
        <p>
            <label for="fo_theme_options[btnl]">Buy button link:</label>
            <br>
            <input type="text" name="fo_theme_options[btnl]" id="fo_theme_options[btnl]" size="100" value="<?php echo $options[btnl]; ?>">
        </p>
	<p>
            <label for="fo_theme_options[tbtnl]">Try button link:</label>
            <br>
            <input type="text" name="fo_theme_options[tbtnl]" id="fo_theme_options[tbtnl]" size="100" value="<?php echo $options[tbtnl]; ?>">
        </p>
        <p>
            <label for="fo_theme_options[twitter]">Twitter account:</label>
            <br>
            <input type="text" name="fo_theme_options[twitter]" id="fo_theme_options[twitter]" size="100" value="<?php echo $options[twitter]; ?>" placeholder="@nickname">
        </p>
        <p>
            <input type="submit" value="Save Changes">
        </p>
    </form>
  <?php
}

?>