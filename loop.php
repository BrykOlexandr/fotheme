<?php if (have_posts()) : ?>
  <?php
  while (have_posts()) {
    the_post();
    $this_post_meta = get_post_meta(get_the_ID());
    $this_section_id = $this_post_meta['section_id'][0];
    $section_tag = 'section';
    if (strpos($this_section_id, '#') === false) {
      $section_id = $this_section_id;
    } else {
      $idArray = explode('#', $this_section_id);
      $section_id = $idArray[1];
      $section_tag = $idArray[0];
    }

    echo '<p style="top: 65px;right: 0;position: fixed;margin: 0;padding: 0 20px;background-color: #ccc;border: 1px solid #fff;">';
    edit_post_link('Edit');
    echo '</p>';

    echo '<' . $section_tag . ' id="' . $section_id . '">';
    the_content();
    echo '</' . $section_tag . '>';
  }
  ?>
<?php else : ?>
    <div class="section" style="height: calc(100vh - 65px);text-align: center;">
        <h2 class="title" style="padding-top: 35vh;margin-top: 0;">Nothing was found!</h2>
        <p>We apologize, but we did not find anything for you!</p>
    </div>
<?php endif; ?>