<?php
query_posts('posts_per_page=-1&post_type=section&orderby=menu_order&post_parent=0&order=ASC');
if (have_posts()) {
  while (have_posts()) {
    the_post();
    $this_post_meta = get_post_meta(get_the_ID());
    $this_section_id = $this_post_meta['section_id'][0];
    $section_tag = 'section';
    if (strpos($this_section_id, '#') === false) {
      $section_id = $this_section_id;
    } else {
      $idArray = explode('#', $this_section_id);
      $section_id = $idArray[1];
      $section_tag = $idArray[0];
    }
    //edit_post_link('<p><strong>РЕДАКТИРОВАТЬ СЕКЦИЮ</strong></p>');
    echo '<' . $section_tag . ' id="' . $section_id . '">';
    the_content();
    echo '</' . $section_tag . '>';
  }
}
?>