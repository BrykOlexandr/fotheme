<?php $foOptions = get_option('fo_theme_options'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>
<html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>
<html <?php language_attributes(); ?> class="no-js ie lt-ie9">
<![endif]-->
<!--[if IE 9]>
<html <?php language_attributes(); ?> class="no-js ie lt-ie10">
<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php
      global $page, $paged;

      wp_title('|', true, 'right');
      bloginfo('name');

      $site_description = get_bloginfo('description', 'display');
      if ($site_description && (is_home() || is_front_page()))
        echo " | $site_description";

      if ($paged >= 2 || $page >= 2)
        echo ' | ' . sprintf('Page %s', max($paged, $page));

      ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <meta name="description" content="<?php echo $foOptions[mdescr]; ?>">
    <meta name="keywords" content="<?php echo $foOptions[mkey]; ?>">
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
    <link rel="dns-prefetch" href="//www.youtube.com">
    <link rel="dns-prefetch" href="//embed.spotify.com">

  <?php wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js" integrity="sha256-1eSvllkLdrfxD6G8RGF9h+mQuD93Af5+GdPBMNc7uMQ=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" integrity="sha256-3Jy/GbSLrg0o9y5Z5n1uw0qxZECH7C6OQpVBgNFYa0g=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js" integrity="sha256-g6iAfvZp+nDQ2TdTR/VVKJf3bGro4ub5fvWSWVRi2NE=" crossorigin="anonymous"></script>
    <![endif]-->
    <!--<script src="js/vendor/modernizr-2.6.2.min.js"></script>-->
  <?php if ($foOptions[analytics]) {
    echo $foOptions[analytics];
  } ?>
</head>
<body <?php body_class(); ?>>
<?php if ($foOptions[banalytics]) {
  echo $foOptions[banalytics];
} ?>

<!--[if lt IE 7]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-head">
            <button type="button" class="navbar-button" data-toggle-block="fo-navbar-collapse">
                <svg width="38" height="35" xmlns="http://www.w3.org/2000/svg">
                    <rect width="100%" height="7" fill="#e6e6e6"/>
                    <rect width="100%" height="7" y="14" fill="#e6e6e6"/>
                    <rect width="100%" height="7" y="28" fill="#e6e6e6"/>
                </svg>
            </button>
			<a type="button" class="btn btn-default btn-checkout btn-try" href="<?php echo $foOptions[tbtnl]; ?>" target="_blank" rel="nofollow"><?php echo $foOptions[tbtn]; ?></a>
            <a type="button" class="btn btn-default btn-checkout" href="<?php echo $foOptions[btnl]; ?>" target="_blank" rel="nofollow"><?php echo $foOptions[btn]; ?></a>
            <a class="navbar-brand navbar-brand-product">
                <img src="<?php echo $foOptions[plogo]; ?>" alt="<?php bloginfo('name'); ?>">
            </a>
            <a class="navbar-brand navbar-brand-company" target="_blank" href="<?php echo $foOptions[clink]; ?>" rel="nofollow">
                <img src="<?php echo $foOptions[clogo]; ?>" alt="<?php bloginfo('description'); ?> Logo">
            </a>
        </div>
        <div class="navbar-body navbar-right" id="fo-navbar-collapse">
          <?php wp_nav_menu(['menu_class' => 'nav navbar-nav', 'container' => false, 'theme_location' => 'main_menu']); ?>
            <a type="button" class="btn btn-default btn-checkout btn-try" href="<?php echo $foOptions[tbtnl]; ?>" target="_blank" rel="nofollow"><?php echo $foOptions[tbtn]; ?></a>
            <a type="button" class="btn btn-default btn-checkout" href="<?php echo $foOptions[btnl]; ?>" target="_blank" rel="nofollow"><?php echo $foOptions[btn]; ?></a>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>