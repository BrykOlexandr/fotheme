<?php get_header(); ?>

<?php if (is_home()): ?>
  <?php get_template_part('loop', 'sections'); ?>
<?php else: ?>
  <?php get_template_part('loop', 'index'); ?>
<?php endif; ?>

<?php get_footer(); ?>