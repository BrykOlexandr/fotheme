<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function fo_setup() {
  // Remove WP version number in <head>
  remove_action('wp_head', 'wp_generator');

  // Remove admin bar
  add_filter('show_admin_bar', '__return_false');

  // Add your "Main Navigation" locations
  register_nav_menus([
    'main_menu' => 'Main Navigation',
  ]);

  // Add support for custom background color
  add_theme_support('custom-background');

  // Add support for custom header
  add_theme_support('custom-header');

  // Add support for logo
//  add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'fo_setup');

// jquery enable
if (!is_admin()) {
  wp_enqueue_script('jquery');
}

require_once(TEMPLATEPATH . '/functions/general-options.php');
require_once(TEMPLATEPATH . '/functions/sections.php');
require_once(TEMPLATEPATH . '/functions/theme-options.php');
require_once(TEMPLATEPATH . '/functions/shortcodes.php');
?>