jQuery.noConflict();
// jQuery(function ($) {
(function ($) {
  function resizeSpotify() {
    $("#audiopreview iframe").each(function () {
      $(this).css("height", parseInt($(this).css("width")) + 80);
    });
  }

  function resizeFeatures() {
    var maxHeight = "auto";
    if ($(window).width() > 767) {
      maxHeight = 0;
      $("#features .row .features-area > div").each(function () {
        $(this).css("height", "auto");
        var height = parseInt($(this).css("height"));
        if (maxHeight < height) {
          maxHeight = height;
        }
      });
    }
    $("#features .row .features-area > div").css("height", maxHeight);
  }

  function resizeGalleryItem() {
    $("#supporting-carousel .supporting-item").each(function () {
      var width = parseInt($(this).css("width"));
      var paddingLeft = parseInt($(this).css("padding-left"));
      var paddingRight = parseInt($(this).css("padding-right"));
      var widthImg = parseInt($(this).find("img").css("width"));
      $(this).find(".supporting-item-info").css("width", width - paddingLeft - paddingRight - widthImg - 1);
    });
  }

  $(document).ready(function () {
    resizeSpotify();
    resizeFeatures();

    // Carousel
    $("#supporting-carousel").slick({
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });

    resizeGalleryItem();
    setTimeout(resizeGalleryItem, 2000);

    // slow scroll to site block
    $(".navbar-default .navbar-body .navbar-nav>li>a").click(function (event) {
      $(".navbar-default .navbar-button").trigger("click");
      event.preventDefault();
      var id = $(this).attr('href');
      var top = $(id).offset().top;
      var navHeight = $(".navbar-default").height();
      $('body,html').animate({scrollTop: +top - +navHeight}, 1500);
    });

    // slow scroll to site block with arrow
    $("header .header-down > a").click(function (event) {
      event.preventDefault();
      var id = $(this).attr('href');
      var top = $(id).offset().top;
      var navHeight = $(".navbar-default").height();
      $('body,html').animate({scrollTop: +top - +navHeight}, 1500);
    });

    // slow scroll to site top
    $(".navbar-default .navbar-head .navbar-brand-product").click(function (event) {
      event.preventDefault();
      $('body,html').animate({scrollTop: 0}, 1500);
    });

    // collapse mobile menu
    $(".navbar-button").each(function () {
      var targetID = $(this).data("toggle-block");
      $(this).click(function () {
        $("#" + targetID).toggleClass("navbar-body-show");
      });
    });
  });

  $(window).resize(function () {
    resizeSpotify();
    resizeFeatures();
    resizeGalleryItem();
  });

  // main menu
  var headerHeight = $("#watchvideo").offset().top;
  var nav = $(".navbar-default");
  var navHeight = nav.height();

  if ($(window).scrollTop() > 0) {
    nav.addClass("non-transparent");

    if ($(window).scrollTop() > headerHeight - navHeight) {
      nav.addClass("btn-show");
    } else {
      nav.removeClass("btn-show");
    }
  } else {
    nav.removeClass("non-transparent");
  }

  $(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
      nav.addClass("non-transparent");

      if ($(window).scrollTop() > headerHeight - navHeight) {
        nav.addClass("btn-show");
      } else {
        nav.removeClass("btn-show");
      }
    } else {
      nav.removeClass("non-transparent");
    }
  });
})(jQuery);
// });